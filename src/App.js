import React, { useState } from 'react'
import './App.css'

function App() {
  const [budget, setBudget] = useState('')
  const [closetCarName, setClosetCarName] = useState('')
  const [error, setError] = useState('')

  const carData = [
    // 차량 목업 데이터
    {carNum: 1, modelName: "morning", carName: "모닝", price: 13150000, imgUrl: ""},
    {carNum: 2, modelName: "casper", carName: "캐스퍼", price: 13850000, imgUrl: ""},
    {carNum: 3, modelName: "seltos", carName: "셀토스", price: 26510000, imgUrl: ""},
    {carNum: 4, modelName: "torres", carName: "토레스", price: 27400000, imgUrl: ""},
    {carNum: 5, modelName: "genesis", carName: "제네시스 GV80", price: 69300000, imgUrl: ""},
    {carNum: 6, modelName: "infinity", carName: "인피니티 QX90", price: 76900000, imgUrl: ""},
    {carNum: 7, modelName: "jeep", carName: "지프 랭글러", price: 83900000, imgUrl: ""},
    {carNum: 8, modelName: "porsche", carName: "포르쉐 911", price: 171100000, imgUrl: ""},
    {carNum: 9, modelName: "lamborghini", carName: "람보르기니 아벤타도르", price: 570000000, imgUrl: ""}
  ]

  // input 값 넘기기
  const handleBudgetChange = e => setBudget(e.target.value)

  const calculateCar = () => {
    // 부가금액
    const adjustedPrice = ( budget - 400000 ) / 1.07

    // 차량 데이터에서 0번 째
    let closestCar = carData[0]

    // 0번 째 차량 데이터 차량 금액에서 부가 금액 뺌, 가장 적은 금액의 차량
    let minDiff = Math.abs(carData[0].price - adjustedPrice )

    for ( let i = 0; i < carData.length; i++ ) {
      // diff = i번 째 차량 데이터의 차량 금액  - 부가금액
      const diff = Math.abs(carData[i].price - adjustedPrice )

      // diff 가 가장 적은 금액의 차량보다 작으면서 i번째 차량 금액이 부가금액 보다 작거나 같으면
      if ( diff < minDiff && carData[i].price <= adjustedPrice ) {
        minDiff = diff
        closestCar = carData[i]
      }
    }

    // 0번 째 데이터의 차량 금액이 고객 예산보다 크다면 에러
    if ( closestCar.price > budget ) {
      setError('추천 차량이 없습니다ㅜㅜ')

    // 크지 않다면 추천 차량 이름 반환
    } else {
      setClosetCarName( closestCar.carName )
    }
  }

  return (
    <div className="hsb-app">
      <div className="hsb-app-title">
        <h1 className="hsb-title">차량 추천 사이트</h1>
        <p className="hsb-sub-title">예산에 맞는 차량을 추천해 드립니다.</p>
      </div>
      <p className="hsb-input-title">예산 입력하기</p>
      <div className="hsb-input-section">
        <input className="hsb-input-box" type="number" value={budget} onChange={ handleBudgetChange }/>
        <button className="hsb-btn" onClick={ calculateCar }>추천받기</button>
        { error ? <p className="hsb-error-message">Error : {error}</p> : <p className="hsb-car-name">추천차량 : {closetCarName}</p> }
      </div>
    </div>
  )
}

export default App
